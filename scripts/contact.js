$(function() {
  $('.error').hide();
  
  $("#submit_btn").click(function() {
		// validate and process form
		// first hide any error messages
    $('.error').hide();
		
	  var name = $("#name").val();
		if (name == "") {
      $("#name_error").show();
      $("#name").focus();
      return false;
    }
		var email = $("#email").val();
		if (email == "") {
      $("#email_error").show();
      $("#email").focus();
      return false;
    }
		var msg = $("#msg").val();
		if (msg == "") {
      $("#msg_error").show();
      $("#msg").focus();
      return false;
    }
		
		var dataString = 'name='+ name + '&email=' + email + '&msg=' + msg;
		//alert (dataString);return false;
		
		$.ajax({
      type: "POST",
      url: "bin/process.php",
      data: dataString,
      success: function() {
		  $("#ContactDIV div").hide();
        $("#SendingCompleate").show();
      }
     });
    return false;
	});
});
runOnLoad(function(){
  $("#name").select().focus();
});
